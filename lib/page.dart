import 'package:flutter/material.dart';

class Page{

  final String imageFile;
  final String videoKey;
  final int pageNumber;

 Page(this.imageFile,this.videoKey,this.pageNumber);

}